let tab = function (){
    let tabNav = document.querySelectorAll('.tabs-nav__item'),
        tabContent =document.querySelectorAll('.tab'),
        tabName;
    tabNav.forEach(item=> {
        item.addEventListener('click',selectTabNav)
    });
    function selectTabNav(){
        tabNav.forEach(item=>{
            item.classList.remove('is-active');
        });
        this.classList.add('is-active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }
    function selectTabContent(tabName){
        tabContent.forEach(item=>{
            item.classList.contains(tabName)? item.classList.add('is-active'): item.classList.remove('is-active');
        })
    }
}
tab()
let tabNav= function (){
    let tabNavItem = document.querySelectorAll('.tabs-nav_item'),
        tabContent =document.querySelectorAll('.tab-work'),
        tabName;
    tabNavItem.forEach(item=>{
        item.addEventListener('click', selectTabNavItem)
    })
    function selectTabNavItem(){
        tabNavItem.forEach(item=>{
            item.classList.remove('is-active');
        });
        this.classList.add('is-active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }
    function selectTabContent(tabName){
        tabContent.forEach(item=>{
            item.classList.contains(tabName)? item.classList.add('is-active'): item.classList.remove('is-active');
        })
    }
}

tabNav()

const btn = document.querySelector('.btn');
let invisible = document.querySelector('.container')
btn.addEventListener('click', () =>{
    invisible.classList.add('show')
    btn.remove()
})

let tabCircle = function (){
    let tabNavCircle = document.querySelectorAll('.tabs-nav__circle'),
        tabContentCircle =document.querySelectorAll('.tab_circle'),
        tabName;

    tabNavCircle.forEach(item=> {
        item.addEventListener('click',selectTabNavCircle)
    });
    function selectTabNavCircle(){
        tabNavCircle.forEach(item=>{
            item.classList.remove('is-active');
        });
        this.classList.add('is-active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContentCircle(tabName);
    }
    function selectTabContentCircle(tabName){
        tabContentCircle.forEach(item=>{
            item.classList.contains(tabName)? item.classList.add('is-active'): item.classList.remove('is-active');
        })
    }
}
tabCircle()


const scrollLeft = document.querySelector('.navigator_left')
const scrollRight = document.querySelector('.navigator_right')
const sections = document.querySelectorAll('.slider-for')
const tabNavCircle = document.querySelectorAll('.img__slider')

let activeItemIndex = 2;

scrollLeft.addEventListener('click',function (){
    activeItemIndex--;

    for (let i = 0; i < sections.length; i++){
        sections[i].classList.remove('is-active');
        tabNavCircle[i].classList.remove('is-active')
    }

    if (activeItemIndex === -1) {
        activeItemIndex = 3;
        sections[activeItemIndex].classList.add('is-active');
        tabNavCircle[activeItemIndex].classList.add('is-active');

    } else {
        sections[activeItemIndex].classList.add('is-active');
        tabNavCircle[activeItemIndex].classList.add('is-active');

    }

})
scrollRight.addEventListener('click',function (){
    activeItemIndex++;

    for (let i = 0; i < sections.length; i++){
        sections[i].classList.remove('is-active');
        tabNavCircle[i].classList.remove('is-active')

    }

    if (activeItemIndex === 4) {
        activeItemIndex = 0;
        sections[activeItemIndex].classList.add('is-active');
        tabNavCircle[activeItemIndex].classList.add('is-active');

    } else {
        sections[activeItemIndex].classList.add('is-active');
        tabNavCircle[activeItemIndex].classList.add('is-active');

    }

})

